package databaseHandler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.DefaultListModel;

import oracle.jdbc.pool.OracleDataSource;

public class Inserter
{

    private OracleDataSource ods;
    private Connection       conn;
    private Boolean          isConnected = false;

    public int connect()
    {
        String database = "ora3inf";
        try
        {
            ods = new OracleDataSource();

            ods.setURL("jdbc:oracle:thin:@ora3.elka.pw.edu.pl:1521:" + database);
            ods.setUser("jkomjago");
            ods.setPassword("jkomjago");
            conn = ods.getConnection();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return 1;
        }
        isConnected = true;
        return 0;
    }

    public int disconnect()
    {
        try
        {
            conn.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return 1;
        }
        isConnected = false;
        return 0;
    }

    public int insertOsoby(String PESEL, String Nazwisko, String Imie,
            String Telefon, String NIP)
    {
        if (!isConnected)
        {
            connect();
        }
        if (isConnected)
        {
            try
            {
                Statement stmt = conn.createStatement();
                stmt.executeQuery("INSERT INTO OSOBY (PESEL,IMIE,NAZWISKO,TELEFON,FIRMA_NIP) VALUES ('"
                        + PESEL
                        + "','"
                        + Imie
                        + "','"
                        + Nazwisko
                        + "','"
                        + Telefon + "','" + NIP + "')");
                stmt.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                return 1;
            }
        }

        else
        {
            return 20;
        }
        return 0;
    }

    public int insertOsoby(String PESEL, String Nazwisko, String Imie,
            String Telefon)
    {
        if (!isConnected)
        {
            connect();
        }
        if (isConnected)
        {
            try
            {
                Statement stmt = conn.createStatement();
                stmt.executeQuery("INSERT INTO OSOBY (PESEL,IMIE,NAZWISKO,TELEFON) VALUES ('"
                        + PESEL
                        + "','"
                        + Imie
                        + "','"
                        + Nazwisko
                        + "','"
                        + Telefon + "')");
                stmt.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                return 1;
            }
        }

        else
        {
            return 20;
        }
        return 0;
    }

    public int insertFirmy(String NIP, String Nazwa, String Adres)
    {
        if (!isConnected)
        {
            connect();
        }
        if (isConnected)
        {
            try
            {
                Statement stmt = conn.createStatement();
                stmt.executeQuery("INSERT INTO FIRMY (NIP,NAZWA,ADRES) VALUES ('"
                        + NIP + "','" + Nazwa + "','" + Adres + "')");

                stmt.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                return 1;
            }
        }

        else
        {
            return 20;
        }
        return 0;
    }

    public void selectFirmy(DefaultListModel<String> selections)
    {
        ResultSet rset;
        if (!isConnected)
        {
            connect();
        }
        if (isConnected)
        {
            try
            {
                Statement stmt = conn.createStatement();
                rset = stmt.executeQuery("Select * from FIRMY order by NAZWA");
                rset.next();
                selections.clear();
                while (rset.isAfterLast() == false)
                {
                    selections.addElement("" + rset.getString(1) + ' ' + rset.getString(2)
                            + ' ' + rset.getString(3));
                    rset.next();
                }
                stmt.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }

        }

      //  String[] a = lists.toArray(new String[lists.size()]);

    }

    public String[] selectFirma(String substring)
    {
        ResultSet rset;
        String[] ret = new String[3];
        if (!isConnected)
        {
            connect();
        }
        if (isConnected)
        {
            try
            {
                Statement stmt = conn.createStatement();
                rset = stmt.executeQuery("Select * from FIRMY where NIP = "
                        + substring);
                rset.next();
                ret[0] = rset.getString(1);
                ret[1] = rset.getString(2);
                ret[2] = rset.getString(3);
                stmt.close();
                rset.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        return ret;
    }

    public Boolean isFirmaExists(String substring)

    {
        ResultSet rset;
        String nip;
        if (!isConnected)
        {
            connect();
        }
        if (isConnected)
        {
            try
            {
                Statement stmt = conn.createStatement();
                rset = stmt.executeQuery("Select * from FIRMY where NIP = "
                        + substring);
                rset.next();
                nip = rset.getString(1);
                System.out.println(nip);
                stmt.close();
                rset.close();
            }
            catch (SQLException e)
            {
                return false;
            }
            if (nip.length() == 10)
            {
                return true;
            }
        }
        return false;
    }
}
