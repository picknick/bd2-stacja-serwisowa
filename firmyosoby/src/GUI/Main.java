package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import databaseHandler.Inserter;

/**
 * Create the GUI and show it. For thread safety, this method should be invoked
 * from the event dispatch thread.
 */

public class Main
{
    private static Osoba      osPanel;
    private static Firmy      firmNew;
    private static WyborFirmy listPanel;

    public static void main(String[] args)
    {
        // Schedule a job for the event dispatch thread:
        // creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                // Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI()
    {
        // Create and set up the window.
        JFrame frame = new JFrame("Firmy i osoby");
        Inserter ins = new Inserter();
        // ins.connect();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(600, 300));
        // Add a checkbox for "firma"

        JPanel outputPanel = new JPanel(new BorderLayout());
        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel firmPanel = new JPanel(new BorderLayout());
        // Add contents to the window.

        osPanel = new Osoba();
        firmNew = new Firmy();
        listPanel = new WyborFirmy(ins);

        mainPanel.add(osPanel, BorderLayout.NORTH);

        JCheckBox CheckFirm = new JCheckBox("Dodaj Firme");
        CheckFirm.setSelected(false);
        Font font = new Font("SansSerif", Font.PLAIN, 18);
        CheckFirm.setFont(font);
        CheckFirm.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                if (e.getStateChange() == ItemEvent.SELECTED)
                {
                    firmPanel.setVisible(true);
                }
                else
                {
                    firmPanel.setVisible(false);
                }
            }

        });
        firmPanel.setVisible(false);
        firmPanel.add(firmNew, BorderLayout.CENTER);
        firmPanel.add(listPanel, BorderLayout.SOUTH);

        mainPanel.add(CheckFirm, BorderLayout.CENTER);
        mainPanel.add(firmPanel, BorderLayout.SOUTH);

        JButton ok = new JButton("Dodaj do bazy");

        ok.setActionCommand("go");
        ok.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        ok.setToolTipText("Dodaje dane do bazy danych");
        ok.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                // dodaj do bazy jeśli dane sa ok
                if (CheckFirm.isSelected())
                {
                    if (ins.isFirmaExists(firmNew.getString(0)) == false)
                    {
                        String nazwa = firmNew.getString(1);
                        if (ins.insertFirmy(firmNew.getString(0), nazwa,
                                firmNew.getString(2)) == 0)

                        {
                            if (ins.insertOsoby(osPanel.getString(0),
                                    osPanel.getString(1), osPanel.getString(2),
                                    osPanel.getString(3), firmNew.getString(0)) == 0)
                            {
                                listPanel.refresh(ins);
                                Dialog dlg = new Dialog(new JFrame(), "OK",
                                        "Successfully added");

                            }
                            else
                            {
                                Dialog dlg = new Dialog(new JFrame(), "ERROR",
                                        "Error while inserting into OSOBY");

                            }
                        }
                        else
                        {
                            Dialog dlg = new Dialog(new JFrame(), "ERROR",
                                    "Error while inserting into FIRMY");
                        }
                    }
                    else
                    {
                        // firma exists
                        if (ins.insertOsoby(osPanel.getString(0),
                                osPanel.getString(1), osPanel.getString(2),
                                osPanel.getString(3), firmNew.getString(0)) == 0)
                        {
                            Dialog dlg = new Dialog(new JFrame(), "OK",
                                    "Successfully added");
                            listPanel.refresh(ins);
                        }
                        else
                        {
                            Dialog dlg = new Dialog(new JFrame(), "ERROR",
                                    "Error while inserting into OSOBY");

                        }
                        ins.disconnect();
                    }
                }
                else
                {
                    if (ins.insertOsoby(osPanel.getString(0),
                            osPanel.getString(1), osPanel.getString(2),
                            osPanel.getString(3)) == 0)
                    {
                        Dialog dlg = new Dialog(new JFrame(), "OK",
                                "Successfully added");
                    }
                    else
                    {
                        Dialog dlg = new Dialog(new JFrame(), "ERROR",
                                "Error while inserting into OSOBY");
                    }
                    ins.disconnect();
                }
            }
        });

        listPanel.addMListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                JList list = (JList) evt.getSource();
                if (evt.getClickCount() == 2)
                {
                    int index = list.locationToIndex(evt.getPoint());
                    String adder = listPanel.getString(index);

                    String[] insert = ins.selectFirma(adder.substring(0, 10));
                    firmNew.setString(insert);

                }
            }
        });

        outputPanel.add(mainPanel, BorderLayout.NORTH);
        outputPanel.add(ok, BorderLayout.SOUTH);
        frame.add(new JScrollPane(outputPanel));
        // frame.add(downPanel);
        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }
}
