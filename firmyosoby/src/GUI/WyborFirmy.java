package GUI;

import java.awt.Font;
import java.awt.event.MouseAdapter;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import databaseHandler.Inserter;

public class WyborFirmy extends JPanel
{
    /**
	 * 
	 */
    private static final long        serialVersionUID = 1L;
    private Font                     font             = new Font("SansSerif",
                                                              Font.PLAIN, 18);
    private JList<String>            list;
    private DefaultListModel<String> selections;

    public WyborFirmy(Inserter ins)
    {
        super();
        selections = new DefaultListModel<String>();
        ins.selectFirmy(selections);
        list = new JList<String>(selections);
        list.setFont(font);
        //list.setPreferredSize(new Dimension(600, 200));
        add(new JScrollPane(list));

    }

    public void addMListener(MouseAdapter maus)
    {
        list.addMouseListener(maus);
    }

    public String getString(int i)
    {
        return (String) selections.get(i);
    }

    public void refresh(Inserter ins)
    {
        ins.selectFirmy(selections);

    }
}
