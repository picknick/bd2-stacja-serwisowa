/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package GUI;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.MaskFormatter;

/**
 * klasa kreujaca inputy do tabeli firmy
 */
public class Firmy extends JPanel implements PropertyChangeListener {

	private static final long serialVersionUID = -8451348516757594833L;
	// Values for the fields
	private static int ROZMIAR = 3;
	// Labels to identify the fields
	private JLabel[] inputFieldLabel = new JLabel[ROZMIAR];

	// Strings for the labels
	private static String[] inputFieldName = { "NIP", "Nazwa", "Adres" };

	// Fields for data entry
	private JFormattedTextField[] inputField = new JFormattedTextField[ROZMIAR];

	// Formats to format and parse numbers
	// private NumberFormat amountFormat;
	// private NumberFormat percentFormat;

	private Font font = new Font("SansSerif", Font.PLAIN, 18);
	private Font font2 = new Font("SansSerif", Font.BOLD, 20);

	public Firmy() {
		super(new BorderLayout());
		JPanel labelPane = new JPanel(new GridLayout(0, 1));
		JPanel inputPanel = new JPanel(new GridLayout(0, 1));
		// double payment = zmianaWartosci(risk,profit);

		JLabel title = new JLabel("Dane Firmy:");
		title.setFont(font2);
		add(title, BorderLayout.NORTH);

		// Create the labels.
		for (int i = 0; i < ROZMIAR; i++) {
			inputFieldLabel[i] = new JLabel(inputFieldName[i]);
			inputFieldLabel[i].setBorder(BorderFactory.createEmptyBorder(2, 0,
					2, 0));
			inputFieldLabel[i].setFont(font);

			if (i == 0) {
				inputField[i] = new JFormattedTextField(
						createFormatter("##########"));
			} else {
				inputField[i] = new JFormattedTextField();
			}
			inputField[i].setValue("");
			inputField[i].setColumns(20);
			inputField[i].setFont(font);

			inputFieldLabel[i].setLabelFor(inputField[i]);
			labelPane.add(inputFieldLabel[i]);
			inputPanel.add(inputField[i]);

		}

		// setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		add(labelPane, BorderLayout.CENTER);
		add(inputPanel, BorderLayout.LINE_END);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

	}

	/**
	 * Get value
	 */
	public String getString(int i) {
		String ret = "" + inputField[i].getValue();
		return ret;
	}

	public void setString(String[] tab) {
		for (int x = 0; x < ROZMIAR; x++) {
			inputField[x].setValue(tab[x]);
		}
	}

	/**
	 * Setting up formats
	 * 
	 */
	protected MaskFormatter createFormatter(String s) {
		MaskFormatter formatter = null;
		try {
			formatter = new MaskFormatter(s);
		} catch (java.text.ParseException exc) {
			System.err.println("formatter is bad: " + exc.getMessage());
			System.exit(-1);
		}
		return formatter;
	}

}
